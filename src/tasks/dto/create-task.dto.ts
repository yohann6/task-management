import { IsInt, IsNotEmpty, IsString } from 'class-validator';
import { Optional } from '@nestjs/common';
import { Type } from 'class-transformer';

export class CreateTaskDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @Optional()
  @IsInt()
  @Type(() => Number)
  priority: number;
}
