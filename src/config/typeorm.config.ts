import { TypeOrmCoreModule } from '@nestjs/typeorm/dist/typeorm-core.module';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeormConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'taskmanagement',
  autoLoadEntities: true,
  synchronize: true,
};
